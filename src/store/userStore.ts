import { action, makeAutoObservable, observable } from "mobx"



class UserStore{
    @observable secondsPassed = 0

    constructor() {
        makeAutoObservable(this)
    }

    @action increaseTimer() {
        this.secondsPassed += 1
    }
}

export default UserStore