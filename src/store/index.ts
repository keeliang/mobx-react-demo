import { createContext } from 'react'
import UserStore from './userStore'

const StoreContext = createContext({
    userStore: new UserStore()
}
)
export default StoreContext