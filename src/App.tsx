import React from 'react';
import logo from './logo.svg';
import './App.css';
import {View,Change} from './components'

function App() {
  return (
    <div className="App">
      <View></View>
      <Change></Change>
    </div>
  );
}

export default App;
