import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import { action, makeAutoObservable, observable } from 'mobx';
import { observer } from 'mobx-react-lite';
class Timer {
  @observable secondsPassed = 0

  constructor() {
    makeAutoObservable(this)
  }

  @action increaseTimer() {
    this.secondsPassed += 1
  }
}
interface TimerProps{
  timer?:any
}
const TimerView:React.FC<TimerProps> = observer((props) => {
  console.log(props)
  return <span>Seconds passed: {props.timer.secondsPassed}</span>
})

const myTimer = new Timer()
ReactDOM.render(
  <React.StrictMode>
    <App/>
  </React.StrictMode>,
  document.getElementById('root')
);

setInterval(() => {
  myTimer.increaseTimer()
}, 1000)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
