
import { useObserver } from "mobx-react-lite";
import useStores from "../hooks/useStores";


const Change = () => {

    const store = useStores()

    const onChange=()=>{
        store.userStore.increaseTimer()
    }
    return useObserver(() => <button onClick={onChange}>Change</button>)
}

export default Change