import { useObserver } from "mobx-react-lite"
import React from "react"
import useStores from "../hooks/useStores"

const View: React.FC = () => {

    const stores = useStores()
    console.log("view....")

    return useObserver(() => <div>{stores.userStore.secondsPassed}</div>)
}

export default View