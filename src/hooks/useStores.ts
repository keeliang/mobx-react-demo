import { useContext } from "react"
import StoreContext from "../store"


const useStores = () => useContext(StoreContext)

export default useStores